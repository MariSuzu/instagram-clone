Rails.application.routes.draw do
  
  devise_for :users, :controllers => {
    :registrations => 'users/registrations',
    :sessions => 'users/sessions',
    :omniauth_callbacks => 'users/omniauth_callbacks' 
  } 

  devise_scope :user do
    get "sign_in", :to => "users/sessions#new"
    get "sign_out", :to => "users/sessions#destroy" 
  end

  root 'posts#index'
  get 'users/:id', to: 'users#show', as: 'user'
  get 'users/', to: 'users#index', as: 'user_index'
  
  # resources :users, only: %i(index show)
  
  resources :users do
    member do
      get :following, :followers
    end
  end
  
  resources :posts, only: %i(new create index show destroy) do
    resources :photos, only: %i(create)
    resources :likes, only: %i(create destroy)
    resources :comments, only: %i(create destroy)
  end
  
  resources :relationships, only: %i(create destroy)
  resources :notifications, only: :index
  
end
