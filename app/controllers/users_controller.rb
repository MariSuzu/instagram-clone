class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_user,     only: :destroy
  
  def index
    @users = User.paginate(page: params[:page], per_page: 10)
  end
  
  def show
    @user = User.find_by(id: params[:id])
    @posts = Post.where(user: @user.id).order('created_at DESC')
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_index_url
  end
  
  def following
    @title = "フォロー中"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def followers
    @title = "フォロワー"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  private
    
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end
